var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cookieSession = require('cookie-session');
var sassMiddleware = require('node-sass-middleware');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var apisRouter = require('./routes/apis');

var app = express();

// 使用sass middleware
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),    // .sass放在public資料夾
  dest: path.join(__dirname, 'public'),   // compile後css放在public資料夾
  outputStyle: 'compressed',
  indentedSyntax: true, // true = .sass and false = .scss
  debug: true,    // 開啟debug模式
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// 啟用cookieSession
app.use(cookieSession({
  key: 'node',
  secret: 'HelloExpressSESSION'
}))

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/apis', apisRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
